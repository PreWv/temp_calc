<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/application/components/db_actions.php';

use components\db_actions;

if (isset($_GET['delete'])) {
    $db = new db_actions();
    $db->delete();

    header('Location: /');
}
