<?php

namespace components;

use PDO;

class db_actions
{
    // Объект класса PDO
    private $db;

    // Соединение с БД
    public function __construct()
    {
        $dbinfo = require $_SERVER['DOCUMENT_ROOT'] . '/application/core/db.php';
        $this->db = new PDO('mysql:host=' . $dbinfo['host'] . '; dbname=' . $dbinfo['db'], $dbinfo['user'], $dbinfo['pass']);
    }

    // Операции над БД
    public function create($name, $format, $value) :void {
        $create = "INSERT INTO `sensors` (`name`, `format`, `value`) VALUES (:name, :format, :value)";
        $params = [
            ':name' => $name,
            ':format' => $format,
            ':value' => $value
        ];
        $stmt = $this->db->prepare($create);
        $stmt->execute($params);
    }

    public function read() :array {
        $read = $this->db->query("SELECT * FROM `sensors`");
        return $read->fetchAll();
    }

    public function readOne($number) :array {
        $readOne = "SELECT * FROM `sensors` WHERE number = ?";
        $stmt = $this->db->prepare($readOne);
        $stmt->execute([$number]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function update($editedName, $editedFormat, $editedValue, $editedNumber) :void {
        $updateSql = "UPDATE `sensors` SET name=?, format=?, value=? WHERE number=?";
        $updateQuery = $this->db->prepare($updateSql);
        $updateQuery->execute([$editedName, $editedFormat, $editedValue, $editedNumber]);
    }

    public function delete() :void {
        $delete = "DELETE FROM `sensors` WHERE `number` = ?";
        $stmt = $this->db->prepare($delete);
        $stmt->execute([$_GET['delete']]);
    }

}

