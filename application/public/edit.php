<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/application/components/update.php';

?>

<div class="row">

    <div class="col-12">
        <h1><?= 'Изменить датчик #' . $result['number'] ?></h1>
    </div>

    <div class="col-12">
        <form action="?page=edit" method="POST">
            <input type="hidden" name="editedNumber" value="<?= $result['number'] ?>">
            <div>
                <label for="name">Название
                    <input type="text" name="editedName" value="<?= $result['name'] ?>">
                </label>
            </div>

            <div>
                <label for="format">Формат градусов
                    <select name="editedFormat" id="">
                        <option selected value="Цельсий">Цельсий</option>
                        <option value="Фаренгейт">Фаренгейт</option>
                    </select>
                </label>
            </div>

            <div>
                <label for="value">Значение
                    <input type="number" name="editedValue" value="<?= $result['value'] ?>">
                </label>
            </div>

            <button type="submit" name="edit">Сохранить</button>
        </form>
    </div>

</div>