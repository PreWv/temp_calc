<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="/application/public/css/normalize.css">
    <link rel="stylesheet" href="/application/public/css/style.css">
    <title>Калькулятор средней температуры</title>
</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-3">
            <nav class="nav">
                <div class="nav__section">
                    <p class="sensors nav__title">Датчики</p>
                    <ul>
                        <li class="nav__item"><a class="nav__link" href="?page=list">Список</a></li>
                        <li class="nav__item"><a class="nav__link" href="?page=sensor">Добавить датчик</a></li>
                    </ul>
                </div>
                <div class="nav__section">
                    <p class="reports nav__title">Отчет</p>
                    <ul>
                        <li class="nav__item"><a class="nav__link" href="?page=report">Сформировать</a></li>
                        <li class="nav__item"><a class="nav__link" href="?page=history">История</a></li>
                    </ul>
                </div>
            </nav>
        </div>

        <div class="col-9 center-block">

            <?php

            if (isset ($_GET['page'])) {
                $page = $_GET['page'];
            } else {
                $page = 'list';
            }

            require_once $_SERVER['DOCUMENT_ROOT'] . '/application/public/' . $page . '.php';

            ?>

        </div>

    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/c6687ace88.js" crossorigin="anonymous"></script>
</body>
</html>