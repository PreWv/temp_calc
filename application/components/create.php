<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/application/components/db_actions.php';

use components\db_actions;

if (isset($_POST['create'])) {
    $name = $_POST['name'];
    $format = $_POST['format'];
    $value = $_POST['value'];

    $db = new db_actions();
    $db->create($name, $format, $value);

    header('Location: /');
}