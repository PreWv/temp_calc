<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/application/components/read.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/application/components/delete.php';

?>

<div class="row">

    <div class="col-12 ">
        <h1>Список датчиков</h1>
    </div>

    <div class="col-12">

        <table class="table shadow">
            <tr class="table-primary">
                <th>Номер</th>
                <th>Название</th>
                <th>Единица измерения</th>
                <th>Значение</th>
                <th>Действия</th>
            </tr>

            <?php foreach ($result as $item) : ?>

                <tr>
                    <td><?= $item['number'] ?></td>
                    <td><?= $item['name'] ?></td>
                    <td><?= $item['format'] ?></td>
                    <td><?= $item['value'] ?></td>
                    <td>
                        <a href="?page=edit&number=<?= $item['number'] ?>" class="btn btn-sm"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-sm" href="?delete=<?= $item['number'] ?>"><i class="fa fa-trash-alt"></i></a>
                    </td>
                </tr>

            <?php endforeach; ?>
        </table>

    </div>
</div>