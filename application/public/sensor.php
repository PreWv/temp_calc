<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/application/components/create.php'; ?>

<div class="row">

    <div class="col-12">
        <h1>Добавить датчик</h1>
    </div>

    <div class="col-12">
        <form action="?page=sensor" method="POST">
            <div>
                <label for="name">Название
                    <input type="text" name="name">
                </label>
            </div>

            <div>
                <label for="format">Формат градусов
                    <select name="format" id="">
                        <option selected value="Цельсий">Цельсий</option>
                        <option value="Фаренгейт">Фаренгейт</option>
                    </select>
                </label>
            </div>

            <div>
                <label for="value">Значение
                    <input type="number" name="value">
                </label>
            </div>

            <button type="submit" name="create">Сохранить</button>
        </form>
    </div>

</div>