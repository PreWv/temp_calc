<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/application/components/db_actions.php';

use components\db_actions;

$db = new db_actions();

if (isset($_GET['number'])) {
    $result = $db->readOne($_GET['number']);
}

if (isset($_POST['edit'])) {
    $editedName = $_POST['editedName'];
    $editedFormat = $_POST['editedFormat'];
    $editedValue = $_POST['editedValue'];
    $editedNumber = $_POST['editedNumber'];

    $db->update($editedName, $editedFormat, $editedValue, $editedNumber);

    header('Location: '. $_SERVER['PHP_SELF']);
}
