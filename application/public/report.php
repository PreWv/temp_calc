<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/application/components/read.php'; ?>

<div class="row">

    <div class="col-12 ">
        <h1>Сформировать отчет</h1>
    </div>

    <div class="col-12">
        <form action="?page=report" method="POST">
            <div>
                <label for="format">Формат градусов
                    <select name="format" id="">
                        <option selected value="Цельсий">Цельсий</option>
                        <option value="Фаренгейт">Фаренгейт</option>
                    </select>
                </label>
            </div>

            <button type="submit" name="createReport">Сформировать</button>
        </form>
    </div>

</div>

<?php

$f = 68;
$c = 5/9 * ($f - 32);

echo $c;

?>

<div class="row">

    <div class="col-12 ">
        <h1>Список датчиков</h1>
    </div>

    <div class="col-12">

        <table class="table shadow">
            <tr class="table-primary">
                <th>Номер</th>
                <th>Название</th>
                <th>Единица измерения</th>
                <th>Значение</th>
            </tr>

            <?php foreach ($result as $item) : ?>

                <tr>
                    <td><?= $item['number'] ?></td>
                    <td><?= $item['name'] ?></td>
                    <td><?= $item['format'] ?></td>
                    <td><?= $item['value'] ?></td>
                </tr>

            <?php endforeach; ?>
        </table>

    </div>
</div>
